//
//  NetworkParams.swift
//  20220825-TrustinHarris-NYCSchools
//
//  Created by Consultant on 8/25/22.
//

import Foundation

enum NetworkParams {
    
    case schoolsList
    case scoresList
    
    var url: URL? {
        switch self {
        case .schoolsList:
            return URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
        case .scoresList:
            return URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")
        }
    }
}
