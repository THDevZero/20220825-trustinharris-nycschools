//
//  NetworkError.swift
//  20220825-TrustinHarris-NYCSchools
//
//  Created by Consultant on 8/25/22.
//

import Foundation

enum NetworkError: Error {
    case invalidURL
    case badData
    case invalidStatusCode(Int)
    case decodeError(Error)
    case other(Error)
}
