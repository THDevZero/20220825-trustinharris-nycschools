//
//  Extensions.swift
//  20220825-TrustinHarris-NYCSchools
//
//  Created by Consultant on 8/25/22.
//

import Foundation
import UIKit

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.Selector.selectedSegmentIndex == 0 {
            performSegue(withIdentifier: schoolDetailIdentifier, sender: self)
        } else {
            showAlert(index: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.schoolsViewModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        if self.Selector.selectedSegmentIndex == 0 {
            cell.textLabel?.text = self.schoolsViewModel.schoolName(for: indexPath.row)?.capitalized
        } else {
            cell.textLabel?.text = self.scoresViewModel.scoresName(for: indexPath.row)?.capitalized
        }
        return cell
        
    }
}
