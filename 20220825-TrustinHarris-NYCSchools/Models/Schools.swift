//
//  Schools.swift
//  20220825-TrustinHarris-NYCSchools
//
//  Created by Consultant on 8/25/22.
//

import Foundation

struct Schools: Codable {
    let name:String
    let overview:String
    let email:String?
    let website:String
    let number:String
    let latitude:String?
    let longitude:String?
    
    enum CodingKeys: String, CodingKey {
        case name = "school_name"
        case overview = "overview_paragraph"
        case email = "school_email"
        case website = "website"
        case number = "phone_number"
        case latitude = "latitude"
        case longitude = "longitude"
    }
}
