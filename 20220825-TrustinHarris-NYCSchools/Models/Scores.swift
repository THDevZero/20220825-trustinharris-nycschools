//
//  Scores.swift
//  20220825-TrustinHarris-NYCSchools
//
//  Created by Consultant on 8/25/22.
//

import Foundation

struct Scores:Codable {
    let schoolName:String
    let numOfTesters:String?
    let readingScore:String?
    let mathScore:String?
    let writingScore:String?
    
    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case numOfTesters = "num_of_sat_test_takers"
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
}
