//
//  SchoolsViewModel.swift
//  20220825-TrustinHarris-NYCSchools
//
//  Created by Consultant on 8/25/22.
//

import Foundation

protocol SchoolsViewModelProtocol {
    var count: Int { get }
    func schoolName(for index: Int) -> String?
    func schoolOverview(for index: Int) -> String?
    func schoolEmail(for index: Int) -> String?
    func schoolWebsite(for index: Int) -> String?
    func schoolNumber(for index: Int) -> String?
    func schoolLatitude(for index: Int) -> String?
    func schoolLongtitude(for index: Int) -> String?
    func getSchoolInfo(for index: Int) -> [String?]
    func requestSchools(completed: @escaping ()-> ())
}

class SchoolListViewModel: SchoolsViewModelProtocol {
    
    private let networkService:NetworkService
    private var schools: [Schools] = []
    
    init(service: NetworkService = NetworkManager()) {
        self.networkService = service
    }
    
    func requestSchools(completed: @escaping ()-> ()) {
        self.networkService.getModel(url: NetworkParams.schoolsList.url) { [weak self] (result: Result<[Schools], NetworkError>) in
            
            switch result {
            case .success(let schools):
                DispatchQueue.main.async {
                    self?.schools = schools
                    completed()
                }
            case .failure(let error):
                // TODO: Make more robust error handling
                print(error)
            }
        }
    }
    
    var count: Int {
        return self.schools.count
    }
    
    func schoolName(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.schools[index].name
    }
    
    func schoolOverview(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.schools[index].overview
    }
    
    func schoolEmail(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.schools[index].email
    }
    
    func schoolWebsite(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.schools[index].website
    }
    
    func schoolNumber(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.schools[index].number
    }
    
    func schoolLatitude(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.schools[index].latitude
    }
    
    func schoolLongtitude(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.schools[index].longitude
    }
    
    func getSchoolInfo(for index: Int) -> [String?] {
        guard index < self.count else { return [] }
        let name = schoolName(for: index)
        let website = schoolWebsite(for: index)
        let email = schoolEmail(for: index)
        let overview = schoolOverview(for: index)
        
        return [name, email, website, overview]
    }
}
