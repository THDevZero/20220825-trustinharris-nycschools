//
//  ScoresViewModel.swift
//  20220825-TrustinHarris-NYCSchools
//
//  Created by Consultant on 8/25/22.
//

import Foundation

protocol ScoresViewModelProtocol {
    var count: Int { get }
    func scoresName(for index: Int) -> String?
    func numOfTesters(for index: Int) -> String?
    func readingScore(for index: Int) -> String?
    func mathScore(for index: Int) -> String?
    func writingScore(for index: Int) -> String?
    func getAllScores(for index: Int) -> String?
    func requestScores()
}

class ScoresViewModel: ScoresViewModelProtocol {
    
    private let networkService:NetworkService
    private var scores: [Scores] = []
    
    init(service: NetworkService = NetworkManager()) {
        self.networkService = service
    }
    
    func requestScores() {
        self.networkService.getModel(url: NetworkParams.scoresList.url) { [weak self] (result: Result<[Scores], NetworkError>) in
            
            switch result {
            case .success(let scores):
                self?.scores = scores
            case .failure(let error):
                // TODO: Make more robust error handling
                print(error)
            }
        }
    }
    
    var count: Int {
        return self.scores.count
    }
    
    func scoresName(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.scores[index].schoolName
    }
    
    func numOfTesters(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.scores[index].numOfTesters
    }
    
    func readingScore(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.scores[index].readingScore
    }
    
    func mathScore(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.scores[index].mathScore
    }
    
    func writingScore(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.scores[index].writingScore
    }
    
    func getAllScores(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return """
        Reading: \(readingScore(for: index) ?? "N/A")
        Math: \(mathScore(for: index) ?? "N/A")
        Writing: \(writingScore(for: index) ?? "N/A")
        """
    }
    
    
}
