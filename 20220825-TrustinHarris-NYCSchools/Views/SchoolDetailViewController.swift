//
//  SchoolDetailViewController.swift
//  20220825-TrustinHarris-NYCSchools
//
//  Created by Consultant on 8/25/22.
//

import UIKit

class SchoolDetailViewController: UIViewController {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var webiteLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    var schoolInfo:[String?] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.nameLabel.text = schoolInfo[0]
        self.emailLabel.text = schoolInfo[1]
        self.webiteLabel.text = schoolInfo[2]
        self.overviewLabel.text = schoolInfo[3]
        
        let tapGesture = UITapGestureRecognizer()
        self.webiteLabel.addGestureRecognizer(tapGesture)
        tapGesture.addTarget(self, action: #selector(openURL))
    }
    
    @objc func openURL() {
        print("clicked")
        let baseUrl = "https://\(schoolInfo[2] ?? "")"
        if let url = URL(string: baseUrl ) {
            UIApplication.shared.open(url) { success in
                if success {
                    print("Success! Opened url \(url)")
                } else {
                    print("Invalid url \(url)")
                }
            }
        }
    }
}
