//
//  ViewController.swift
//  20220825-TrustinHarris-NYCSchools
//
//  Created by Consultant on 8/25/22.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var Selector: UISegmentedControl!
    @IBOutlet weak var ListTableView: UITableView!
    
    let schoolsViewModel:SchoolsViewModelProtocol = SchoolListViewModel()
    let scoresViewModel:ScoresViewModelProtocol = ScoresViewModel()
    let schoolDetailIdentifier = "gotoSchoolsDetail"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTableView()
    }
    
    @IBAction func selectorPressed(_ sender: Any) {
        self.ListTableView.reloadData()
    }
    
    func setTableView() {
        ListTableView.dataSource = self
        ListTableView.delegate = self
        
        self.schoolsViewModel.requestSchools {
            self.ListTableView.reloadData()
        }
        self.scoresViewModel.requestScores()
    }
    
    func showAlert(index: Int) {
        let title = self.scoresViewModel.scoresName(for: index)
        let message = self.scoresViewModel.getAllScores(for: index)
        let alert = UIAlertController(title: title , message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel))
        present(alert, animated: true)
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == schoolDetailIdentifier {
            if let destination = segue.destination as? SchoolDetailViewController {
                destination.schoolInfo = self.schoolsViewModel.getSchoolInfo(for: ListTableView.indexPathForSelectedRow?.row ?? 0)
            }
        }
    }
}
